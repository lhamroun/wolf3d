/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strichr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/21 04:58:49 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/11/21 05:03:07 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strichr(char const *s, int c, int i)
{
	char	*tmp;

	tmp = (char *)s;
	if (i < 0 || i > (int)ft_strlen(tmp))
		return (NULL);
	while (tmp[i])
	{
		if (tmp[i] == c)
			return (tmp + i);
		++i;
	}
	return (NULL);
}
