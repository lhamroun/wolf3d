/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_fd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 02:02:59 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/12/12 02:03:57 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*read_fd(int fd)
{
	int		i;
	int		ret;
	char	*line;
	char	*tmp;

	i = 0;
	if (fd == -1 || !(line = ft_strnew(0)))
		return (NULL);
	if (!(tmp = ft_memalloc(sizeof(char) * BUFF_SIZE + 1)))
	{
		ft_strdel(&line);
		return (NULL);
	}
	while ((ret = (int)read(fd, tmp, BUFF_SIZE)) > 0)
	{
		tmp[ret] = '\0';
		if (!(line = ft_strjoinfree(line, tmp, 1)))
		{
			ft_strdel(&line);
			ft_strdel(&tmp);
			return (NULL);
		}
	}
	ft_strdel(&tmp);
	return (line);
}
