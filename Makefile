# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/21 02:37:03 by lyhamrou          #+#    #+#              #
#    Updated: 2020/02/08 08:05:24 by lyhamrou         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = wolf3d
C_FLAGS = -Wall -Werror -Wextra -MMD
CC = gcc

MLX_PATH = minilibx_macos/
LD_LIBS = -L libft/ -lft
MLX_LD_LIBS = -lmlx -L $(MLX_PATH) -framework AppKit -framework OpenGl
INCLUDE = -I includes -I libft/ -I $(MLX_PATH)
HEADER = includes/wolf3d.h

SRC_PATH = srcs/
SRC_NAME = main.c init.c parsing.c check_parsing.c wolf.c event_key.c \
		   raycasting.c move.c texture.c test.c mini_map.c
SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))
OBJ_PATH = .obj/
OBJ_NAME = $(SRC_NAME:.c=.o)
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))

LIBFT_A = libft/libft.a
MLX_A = libmlx.a
LIBMLX_A = $(addprefix $(MLX_PATH),$(MLX_A))

all: $(NAME)

$(NAME): $(OBJ_PATH) $(LIBFT_A) $(OBJ)
	$(CC) $(FLAGS) $(INCLUDE) $(MLX_LD_LIBS) $(LD_LIBS) $(OBJ) -o $(NAME)

$(OBJ_PATH):
	mkdir -p $(OBJ_PATH)
	make -C $(MLX_PATH)

$(LIBFT_A):
	make -C libft/

$(OBJ_PATH)%.o: $(SRC_PATH)%.c $(HEADER)
	$(CC) $(FLAGS) $(INCLUDE) -o $@ -c $<

clean:
	$(RM) -rf $(OBJ_PATH)
	make clean -C libft/
#	make clean -C $(MLX_PATH)

fclean: clean
	$(RM) -rf $(NAME)
	make fclean -C libft/

re: fclean all

norme: fclean
	norminette libft srcs includes

save: fclean
	@rm -rf *.swp
	@git add .
	@git commit -m "Auto-save"
	@git push

.PHONY: all dodge clean fclean re save
