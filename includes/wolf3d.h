/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/21 02:24:27 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:14:17 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <math.h>
# include "libft.h"
# include "mlx.h"

/*
**		_______________________________________________________________________
**		_______________________________________________________________SETTINGS
**		_______________________________________________________________________
*/
# define FOV 1
# define CAM_SPEED 0.06
# define ROT_SPEED 0.06283
# define PAS 50

/*
**		_______________________________________________________________________
**		_________________________________________________________________COLORS
**		_______________________________________________________________________
*/
# define BLEU "\033[34m"
# define VERT "\033[32m"
# define JAUNE "\033[33m"
# define ROUGE "\033[31m"
# define BLANC "\033[0m"
# define SNART 0xff000000
# define TRANS 0x000000ff
# define WHITE 0xffffff
# define GREY 0xB9ACA9
# define GREEN 0x82DC73
# define BLUE 0x3368E1
# define YELLOW 0xECD72B
# define RED 0xB6371C
# define BLACK 0x000000
# define BROWN 0x846D11
# define BDX 0x543002
# define KAKI 0xC2E720

/*
**		_______________________________________________________________________
**		_______________________________________________________________KEY CODE
**		_______________________________________________________________________
*/
# define KEY_Q 12
# define KEY_W 13
# define KEY_T 17
# define KEY_S 1
# define KEY_ESC 53
# define KEY_LEFT 123
# define KEY_RIGHT 124
# define KEY_DOWN 125
# define KEY_UP 126
# define KEY_SHIFT 257

/*
**		_______________________________________________________________________
**		______________________________________________________________MASK CODE
**		_______________________________________________________________________
*/
# define KEY_PRESS 2
# define KEY_RELEASE 3
# define MASK_UP 1
# define MASK_DOWN 2
# define MASK_RIGHT 4
# define MASK_LEFT 8

/*
**		_______________________________________________________________________
**		_________________________________________________________MLX PARAMETERS
**		_______________________________________________________________________
*/
# define WIN_X 960
# define WIN_Y 600
# define MINI_MAPXY 250
# define MMXY 250
# define TITLE "wolf3d"
# define SKY "assets/sky.xpm"
# define SOL "assets/sol.xpm"
# define EST "assets/est.xpm"
# define NORD "assets/nord.xpm"
# define OUEST "assets/ouest.xpm"
# define SUD "assets/sud.xpm"

typedef enum	e_bool
{
	FALSE,
	TRUE
}				t_bool;

typedef struct	s_texture
{
	int			h1;
	int			h2;
	int			hn;
	int			hs;
	int			he;
	int			ho;
	int			l1;
	int			l2;
	int			ln;
	int			ls;
	int			le;
	int			lo;
	int			bppn;
	int			*datan;
	int			*datas;
	int			*datae;
	int			*datao;
	void		*n;
	void		*s;
	void		*e;
	void		*o;
	void		*sol;
	void		*sky;
}				t_texture;

typedef struct	s_camera
{
	int			mapx;
	int			mapy;
	float		posx;
	float		posy;
	float		dirx;
	float		diry;
	float		px;
	float		py;
}				t_camera;

typedef struct	s_ray
{
	int			start;
	int			end;
	int			stepx;
	int			stepy;
	int			hit;
	int			side;
	float		camx;
	float		walldist;
	float		posx;
	float		posy;
	float		dirx;
	float		diry;
	float		distx;
	float		disty;
	float		dx;
	float		dy;
}				t_ray;

typedef struct	s_mlx
{
	int			pas;
	int			sl;
	int			slmm;
	int			edn;
	int			bpp;
	int			*data;
	int			*datamm;
	void		*mlx;
	void		*win;
	void		*img;
	void		*imgmm;
}				t_mlx;

typedef struct	s_env
{
	char		start;
	char		run;
	short		frame;
	int			mm;
	int			shift;
	int			n;
	float		rot;
	float		speed;
	float		rot_speed;
	char		**map;
	t_mlx		mlx;
	t_ray		ray;
	t_texture	txt;
	t_camera	cam;
}				t_env;

void			print_env(t_env *env);
void			print_camera(t_env *env);
void			print_ray(t_env *env);
void			print_texture(t_env *env);
void			init_env(t_env *env);
void			init_camera(t_env *env);
void			init_texture(t_env *env);
void			init_mlx(t_env *env);
void			init_ray(t_env *env);
void			init_mini_map(t_env *env);
int				parsing(int fd, t_env *env);
int				check_if_map_is_surrounded(t_env *env);
int				wolf(t_env *env);
int				event_key_press(int key, t_env *env);
int				event_key_release(int key, t_env *env);
int				event_loop(t_env *env);
int				render(t_env *env);
int				ft_quit(t_env *env);
void			cleaner(t_env *env);
void			raycasting(int x, t_env *env);
void			ft_move(t_env *env);
void			get_texture(t_env *env, int x, int height);
void			mini_map(t_env *env);

#endif
