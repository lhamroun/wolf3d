/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/08 07:37:08 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:17:12 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		calc_texture(t_env *env, int tx, int x, int height)
{
	int		y;
	int		color;
	int		ty;

	y = env->ray.start;
	while (y < env->ray.end)
	{
		ty = (y * 2 - WIN_Y + height)
			* ((env->txt.ho / sizeof(int)) / 2) / height;
		if (env->ray.side == 0 && env->ray.dirx > 0)
			color = env->txt.datae[ty * env->txt.le + tx];
		else if (env->ray.side == 0)
			color = env->txt.datan[ty * env->txt.ln + tx];
		if (env->ray.side == 1 && env->ray.diry > 0)
			color = env->txt.datao[ty * env->txt.lo + tx];
		else if (env->ray.side == 1)
			color = env->txt.datas[ty * env->txt.ls + tx];
		env->mlx.data[y * (env->mlx.sl / sizeof(int)) + x] = color;
		++y;
	}
}

void		get_texture(t_env *env, int x, int height)
{
	float	wallx;
	int		tx;

	if (env->ray.side == 1)
		wallx = env->ray.posx + ((env->cam.mapy - env->ray.posy
				+ (1 - env->ray.stepy) / 2) / env->ray.diry) * env->ray.dirx;
	else
		wallx = env->ray.posy + ((env->cam.mapx - env->ray.posx
				+ (1 - env->ray.stepx) / 2) / env->ray.dirx) * env->ray.diry;
	wallx -= (int)wallx;
	tx = (int)(wallx * (env->txt.ln / sizeof(int)));
	if (env->ray.side == 0 && env->ray.dirx > 0)
		tx = (env->txt.ln / sizeof(int)) - tx - 1;
	if (env->ray.side == 1 && env->ray.dirx < 0)
		tx = (env->txt.ln / sizeof(int)) - tx - 1;
	calc_texture(env, tx, x, height);
}
