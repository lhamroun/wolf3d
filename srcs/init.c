/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/21 03:04:49 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:12:46 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		init_ray(t_env *env)
{
	env->ray.camx = 0;
	env->ray.start = 0;
	env->ray.end = 0;
	env->ray.walldist = 0.0;
	env->ray.hit = 0;
	env->ray.side = 0;
	env->ray.dx = 0;
	env->ray.dy = 0;
	env->ray.distx = 0;
	env->ray.disty = 0;
	env->ray.dirx = 0;
	env->ray.diry = 0;
	env->ray.posx = env->cam.posx;
	env->ray.posy = env->cam.posy;
	env->ray.stepx = 0;
	env->ray.stepy = 0;
}

void		init_texture(t_env *env)
{
	env->txt.n = mlx_xpm_file_to_image(env->mlx.mlx, NORD, &env->txt.ln,
			&env->txt.hn);
	env->txt.s = mlx_xpm_file_to_image(env->mlx.mlx, SUD, &env->txt.ls,
			&env->txt.hs);
	env->txt.e = mlx_xpm_file_to_image(env->mlx.mlx, EST, &env->txt.le,
			&env->txt.he);
	env->txt.o = mlx_xpm_file_to_image(env->mlx.mlx, OUEST, &env->txt.lo,
			&env->txt.ho);
	env->txt.sky = mlx_xpm_file_to_image(env->mlx.mlx, SKY, &env->txt.l2,
			&env->txt.h2);
	env->txt.sol = mlx_xpm_file_to_image(env->mlx.mlx, SOL, &env->txt.l1,
			&env->txt.h1);
	env->txt.datan = (int *)mlx_get_data_addr(env->txt.n, &env->mlx.bpp,
			&env->txt.ln, &env->mlx.edn);
	env->txt.datae = (int *)mlx_get_data_addr(env->txt.e, &env->mlx.bpp,
			&env->txt.le, &env->mlx.edn);
	env->txt.datas = (int *)mlx_get_data_addr(env->txt.s, &env->mlx.bpp,
			&env->txt.ls, &env->mlx.edn);
	env->txt.datao = (int *)mlx_get_data_addr(env->txt.o, &env->mlx.bpp,
			&env->txt.lo, &env->mlx.edn);
}

void		init_camera(t_env *env)
{
	int		i;
	int		j;

	i = 1;
	env->cam.mapx = 0;
	env->cam.mapy = 0;
	env->cam.dirx = -1;
	env->cam.diry = 0;
	env->cam.px = 0;
	env->cam.py = FOV;
	while (env->map[i])
	{
		j = 1;
		while (env->map[i][j + 1])
		{
			if (env->map[i][j] == '0')
			{
				env->cam.posx = i + 0.5;
				env->cam.posy = j + 0.5;
				return ;
			}
			++j;
		}
		++i;
	}
}

void		init_mlx(t_env *env)
{
	env->mlx.mlx = mlx_init();
	env->mlx.win = mlx_new_window(env->mlx.mlx, WIN_X, WIN_Y, TITLE);
	env->mlx.img = mlx_new_image(env->mlx.mlx, WIN_X, WIN_Y);
	env->mlx.data = (int *)
	mlx_get_data_addr(env->mlx.img, &env->mlx.bpp, &env->mlx.sl, &env->mlx.edn);
}

void		init_env(t_env *env)
{
	env->start = 1;
	env->shift = 0;
	env->run = 0;
	env->mm = 1;
	env->rot = 0;
	env->frame = 0;
	env->speed = CAM_SPEED;
	init_mlx(env);
	init_camera(env);
	init_ray(env);
	init_texture(env);
	init_mini_map(env);
}
