/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/08 08:17:42 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:22:14 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		ft_quit(t_env *env)
{
	mlx_destroy_image(env->mlx.mlx, env->mlx.img);
	mlx_destroy_window(env->mlx.mlx, env->mlx.win);
	cleaner(env);
	return (1);
}

int		render(t_env *env)
{
	int		x;

	x = 0;
	ft_memset(env->mlx.datamm, TRANS, sizeof(int) * MINI_MAPXY * MINI_MAPXY);
	if (env->mm == 1)
		mini_map(env);
	if (env->run != 0 || env->rot != FALSE || env->start == TRUE)
	{
		ft_move(env);
		ft_memset(env->mlx.data, TRANS, sizeof(int) * WIN_X * WIN_Y);
		while (x < WIN_X)
		{
			raycasting(x, env);
			++x;
		}
		env->start = FALSE;
		mlx_put_image_to_window(env->mlx.mlx, env->mlx.win, env->txt.sky, 0, 0);
		mlx_put_image_to_window(env->mlx.mlx, env->mlx.win, env->txt.sol, 0,
				WIN_Y / 2);
		mlx_put_image_to_window(env->mlx.mlx, env->mlx.win, env->mlx.img, 0, 0);
	}
	mlx_put_image_to_window(env->mlx.mlx, env->mlx.win, env->mlx.imgmm, 10, 10);
	return (1);
}

int		wolf(t_env *env)
{
	mlx_do_key_autorepeatoff(env->mlx.mlx);
	mlx_do_sync(env->mlx.mlx);
	mlx_loop_hook(env->mlx.mlx, render, env);
	mlx_hook(env->mlx.win, KEY_PRESS, 0, event_key_press, env);
	mlx_hook(env->mlx.win, KEY_RELEASE, 0, event_key_release, env);
	mlx_loop(env->mlx.mlx);
	return (1);
}
