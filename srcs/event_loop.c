/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_loop.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 02:17:23 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:11:00 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	ft_move(t_env *env)
{
	if (env->run != 0)
	{
		if (env->run & MASK_UP)
		{
			if (env->map[(int)(env->cam.posx + env->cam.dirx * env->speed)][(int)env->cam.posy] == '0')
				env->cam.posx += env->cam.dirx * env->speed;
			if (env->map[(int)env->cam.posx][(int)(env->cam.posy + env->cam.diry * env->speed)] == '0')
				env->cam.posy += env->cam.diry * env->speed;
		}
		if (env->run & MASK_DOWN)
		{
			if (env->map[(int)(env->cam.posx - env->cam.dirx * env->speed)][(int)env->cam.posy] == '0')
				env->cam.posx -= env->cam.dirx * env->speed;
			if (env->map[(int)env->cam.posx][(int)(env->cam.posy - env->cam.diry * env->speed)] == '0')
				env->cam.posy -= env->cam.diry * env->speed;
		}
		if (env->run & MASK_RIGHT)
		{
			if (env->map[(int)(env->cam.posx + env->cam.px * env->speed)][(int)env->cam.posy] == '0')
				env->cam.posx += env->cam.px * env->speed;
			if (env->map[(int)env->cam.posx][(int)(env->cam.posy + env->cam.py * env->speed)] == '0')
				env->cam.posy += env->cam.py * env->speed;
		}
		if (env->run & MASK_LEFT)
		{
			if (env->map[(int)(env->cam.posx - env->cam.px * env->speed)][(int)env->cam.posy] == '0')
				env->cam.posx -= env->cam.px * env->speed;
			if (env->map[(int)env->cam.posx][(int)(env->cam.posy - env->cam.py * env->speed)] == '0')
				env->cam.posy -= env->cam.py * env->speed;
		}
	}
	if (env->rot != 0)
	{
		env->cam.olddirx = env->cam.dirx;
		env->cam.dirx = env->cam.dirx * cos(-env->speed) - env->cam.diry * sin(-env->speed);
		env->cam.diry = env->cam.olddirx * sin(-env->speed) + env->cam.diry * cos(-env->speed);
		env->cam.oldpx = env->cam.px;
		env->cam.px = env->cam.px * cos(-env->speed) - env->cam.py * sin(-env->speed);
		env->cam.py = env->cam.oldpx * sin(-env->speed) + env->cam.py * cos(-env->speed);
	}
}

void	draw_column(t_env *env, int i)
{
	int		y;
	int		color;

	y = env->ray.start;
	while (y < env->ray.end)
	{
		color = BLUE;
		if (env->ray.side == 1)
			color = BROWN;
		env->mlx.data[y * (env->mlx.sl / sizeof(int)) + i] = color;
		++y;
	}
}

void	raycaster(t_env *env)
{
	int		i;

	i = 0;
	while (i < WIN_X)
	{
		env->cam.pas = (2 * i / WIN_X) - 1;
		env->ray.posx = env->cam.posx;
		env->ray.posy = env->cam.posy;
		env->ray.dirx = env->cam.dirx + env->cam.px * env->cam.pas;
		env->ray.diry = env->cam.diry + env->cam.py * env->cam.pas;
		from_camera_to_wall(env);
		if (env->ray.side == 0)
			env->ray.walldist = ft_abs((env->cam.mapx - env->ray.posx + (1 - env->ray.stepx) / 2) / env->ray.dirx);
		else
			env->ray.walldist = ft_abs((env->cam.mapy - env->ray.posy + (1 - env->ray.stepy) / 2) / env->ray.diry);
		env->h = ft_abs(WIN_Y / env->ray.walldist);
		env->ray.start = -env->h / 2 + WIN_Y / 2;
		env->ray.end = env->h / 2 + WIN_Y / 2;
		if (env->ray.start < 0)
			env->ray.start = 0;
		if (env->ray.end >= WIN_Y)
			env->ray.end = WIN_Y - 1;
		draw_column(env, i);
		++i;
	}
}

int		event_loop(t_env *env)
{
	if (env->start == TRUE || env->run != 0 || env->rot != FALSE)
	{
		raycaster(env);
		env->start = FALSE;
	}
	mlx_put_image_to_window(env->mlx.mlx, env->mlx.win, env->txt.sky, 0, 0);
	mlx_put_image_to_window(env->mlx.mlx, env->mlx.win, env->txt.sol, 0,
		WIN_Y / 2);
	mlx_put_image_to_window(env->mlx.mlx, env->mlx.win, env->mlx.img, 20, 20);
	return (1);
}
