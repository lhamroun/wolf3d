/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/09 04:59:33 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/01/22 14:29:29 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	ft_move_left_right(t_env *env)
{
	if (env->run & MASK_RIGHT)
	{
		if (env->map[(int)(env->cam.posx + env->cam.diry * env->speed * (env->shift == 1 ? 3 : 1))]
			[(int)env->cam.posy] == '0')
			env->cam.posx
				+= env->cam.px * (env->speed * (env->shift == 1 ? 3 : 1));
		if (env->map[(int)env->cam.posx]
			[(int)(env->cam.posy + env->cam.dirx * env->speed * (env->shift == 1 ? 3 : 1))] == '0')
			env->cam.posy
				+= env->cam.py * (env->speed * (env->shift == 1 ? 3 : 1));
	}
	if (env->run & MASK_LEFT)
	{
		if (env->map[(int)(env->cam.posx - env->cam.diry * env->speed * (env->shift == 1 ? 3 : 1))]
			[(int)env->cam.posy] == '0')
			env->cam.posx
				-= env->cam.px * (env->speed * (env->shift == 1 ? 3 : 1));
		if (env->map[(int)env->cam.posx]
			[(int)(env->cam.posy - env->cam.dirx * env->speed * (env->shift == 1 ? 3 : 1))] == '0')
			env->cam.posy
				-= env->cam.py * (env->speed * (env->shift == 1 ? 3 : 1));
	}
}

void	ft_move_up_down(t_env *env)
{
	if (env->run & MASK_UP)
	{
		if (env->map[(int)(env->cam.posx + env->cam.dirx * env->speed * (env->shift == 1 ? 3 : 1))]
			[(int)env->cam.posy] == '0')
			env->cam.posx
				+= env->cam.dirx * (env->speed * (env->shift == 1 ? 3 : 1));
		if (env->map[(int)env->cam.posx]
			[(int)(env->cam.posy + env->cam.diry * env->speed * (env->shift == 1 ? 3 : 1))] == '0')
			env->cam.posy
				+= env->cam.diry * (env->speed * (env->shift == 1 ? 3 : 1));
	}
	if (env->run & MASK_DOWN)
	{
		if (env->map[(int)(env->cam.posx - env->cam.dirx * env->speed * (env->shift == 1 ? 3 : 1))]
			[(int)env->cam.posy] == '0')
			env->cam.posx
				-= env->cam.dirx * (env->speed * (env->shift == 1 ? 3 : 1));
		if (env->map[(int)env->cam.posx]
			[(int)(env->cam.posy - env->cam.diry * env->speed * (env->shift == 3 ? 3 : 1))] == '0')
			env->cam.posy
				-= env->cam.diry * (env->speed * (env->shift == 1 ? 3 : 1));
	}
}

void	ft_rotation(t_env *env)
{
	float	olddirx;
	float	oldpx;

	olddirx = env->cam.dirx;
	oldpx = env->cam.px;
	env->cam.dirx = olddirx * cos(-env->rot) - env->cam.diry * sin(-env->rot);
	env->cam.diry = olddirx * sin(-env->rot) + env->cam.diry * cos(-env->rot);
	env->cam.px = oldpx * cos(-env->rot) - env->cam.py * sin(-env->rot);
	env->cam.py = oldpx * sin(-env->rot) + env->cam.py * cos(-env->rot);
}

void	ft_move(t_env *env)
{
	if (env->run != 0)
	{
		if (env->run & MASK_UP || env->run & MASK_DOWN)
			ft_move_up_down(env);
		if (env->run & MASK_LEFT || env->run & MASK_RIGHT)
			ft_move_left_right(env);
	}
	if (env->rot != 0)
	{
		ft_rotation(env);
	}
}
//			printf("ssssssssssssssssssuuuuuuuuuuuuuuuuucccccccccccceeeeeeeee\n");
