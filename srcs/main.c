/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/21 02:26:46 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 07:38:09 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		cleaner(t_env *env)
{
	int		i;

	i = 0;
	if (env->map)
	{
		while (env->map[i])
			ft_strdel(&env->map[i++]);
		free(env->map);
		env->map = NULL;
	}
}

static int	error_map(int fd, t_env *env)
{
	ft_putstr("Error map\n");
	return (0);
}

static int	ft_usage(void)
{
	ft_putstr("usage: ./wolf3d [map/filename]");
	return (0);
}

int			main(int ac, char **av)
{
	int		fd;
	t_env	env;

	if (ac != 2 || (fd = open(av[1], O_RDONLY)) == -1)
		return (ft_usage());
	if (parsing(fd, &env) == -1)
		return (error_map(fd, &env));
	init_env(&env);
	wolf(&env);
	cleaner(&env);
	return (0);
}
