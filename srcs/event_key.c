/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_key.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 02:18:13 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:09:17 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		event_key_release(int key, t_env *env)
{
	if (key == KEY_UP)
		env->run ^= MASK_UP;
	else if (key == KEY_DOWN)
		env->run ^= MASK_DOWN;
	else if (key == KEY_LEFT)
		env->run ^= MASK_LEFT;
	else if (key == KEY_RIGHT)
		env->run ^= MASK_RIGHT;
	else if (key == KEY_Q || key == KEY_W)
		env->rot = 0;
	else if (key == KEY_SHIFT)
		env->shift = 0;
	return (1);
}

int		event_key_press(int key, t_env *env)
{
	if (key == KEY_ESC)
	{
		ft_quit(env);
		exit(0);
	}
	else if (key == KEY_UP)
		env->run |= MASK_UP;
	else if (key == KEY_DOWN)
		env->run |= MASK_DOWN;
	else if (key == KEY_LEFT)
		env->run |= MASK_LEFT;
	else if (key == KEY_RIGHT)
		env->run |= MASK_RIGHT;
	else if (key == KEY_Q || key == KEY_W)
	{
		if (key == KEY_Q)
			env->rot += -ROT_SPEED;
		else if (key == KEY_W)
			env->rot += ROT_SPEED;
	}
	if (key == KEY_T)
		env->mm = env->mm == 0 ? 1 : 0;
	else if (key == KEY_SHIFT)
		env->shift = 1;
	return (1);
}
