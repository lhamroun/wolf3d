/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mini_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/08 08:12:53 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:14:53 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	init_mini_map(t_env *env)
{
	int		nb;

	env->mlx.imgmm = mlx_new_image(env->mlx.mlx, MINI_MAPXY, MINI_MAPXY);
	env->mlx.datamm =
		(int *)mlx_get_data_addr(env->mlx.imgmm, &env->mlx.bpp,
				&env->mlx.slmm, &env->mlx.edn);
	ft_memset(env->mlx.datamm, TRANS, sizeof(int) * MINI_MAPXY * MINI_MAPXY);
	nb = tab_2d_len(env->map);
	if (ft_strlen(env->map[0]) > nb)
		nb = ft_strlen(env->map[0]);
	env->mlx.pas = MINI_MAPXY / nb - 1;
}

void	draw_mini_map(int x, int y, t_env *env, int c)
{
	int		i;
	int		j;
	int		coord;

	i = 0;
	coord = y * env->mlx.pas * MINI_MAPXY + x * env->mlx.pas;
	while (i < env->mlx.pas)
	{
		j = 0;
		while (j < env->mlx.pas)
		{
			if (c == '1' && coord + j >= 0 && coord + j < MMXY * MMXY)
				env->mlx.datamm[coord + j] = BLACK;
			else if (c == '0' && coord + j >= 0 && coord + j < MMXY * MMXY)
				env->mlx.datamm[coord + j] = GREEN + 0xaf000000;
			if (i == 0 || i == env->mlx.pas - 1 || j == 0 || j == env->mlx.pas)
				env->mlx.datamm[coord + j] = SNART;
			if ((int)env->cam.posx == x && (int)env->cam.posy == y)
				env->mlx.datamm[coord + j] = RED;
			++j;
		}
		coord += MINI_MAPXY;
		++i;
	}
}

void	draw_rays(t_env *env)
{
	(void)env;
}

void	mini_map(t_env *env)
{
	int		i;
	int		j;

	i = 0;
	while (env->map[i])
	{
		j = 0;
		while (env->map[i][j])
		{
			draw_mini_map(i, j, env, env->map[i][j]);
			++j;
		}
		++i;
	}
	draw_rays(env);
}
