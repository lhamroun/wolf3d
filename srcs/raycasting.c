/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycasting.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/09 00:52:42 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:16:37 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	adjust_view(t_env *env)
{
	if (env->ray.side == 0)
		env->ray.walldist = ((float)env->cam.mapx - env->ray.posx
				+ (1 - env->ray.stepx) / 2) / env->ray.dirx;
	else
		env->ray.walldist = ((float)env->cam.mapy - env->ray.posy
				+ (1 - env->ray.stepy) / 2) / env->ray.diry;
	if (env->ray.walldist < 0)
		env->ray.walldist *= -1;
}

int		calc_column_height(t_env *env)
{
	int	height;

	height = ft_abs(WIN_Y / env->ray.walldist);
	env->ray.start = (int)(-(height / 2) + WIN_Y / 2);
	env->ray.end = (int)(height / 2 + WIN_Y / 2);
	if (env->ray.start < 0)
		env->ray.start = 0;
	if (env->ray.end > WIN_Y)
		env->ray.end = WIN_Y - 1;
	return (height);
}

void	pre_calc_wall_dist(t_env *env)
{
	if (env->ray.dirx < 0)
	{
		env->ray.stepx = -1;
		env->ray.distx = (env->ray.posx - env->cam.mapx) * env->ray.dx;
	}
	else
	{
		env->ray.stepx = 1;
		env->ray.distx = (env->cam.mapx + 1.0 - env->ray.posx) * env->ray.dx;
	}
	if (env->ray.diry < 0)
	{
		env->ray.stepy = -1;
		env->ray.disty = (env->ray.posy - env->cam.mapy) * env->ray.dy;
	}
	else
	{
		env->ray.stepy = 1;
		env->ray.disty = (env->cam.mapy + 1.0 - env->ray.posy) * env->ray.dy;
	}
}

void	calc_wall_dist(t_env *env)
{
	while (env->ray.hit == 0)
	{
		if (env->ray.distx < env->ray.disty)
		{
			env->ray.distx += env->ray.dx;
			env->cam.mapx += (int)env->ray.stepx;
			env->ray.side = 0;
		}
		else
		{
			env->ray.disty += env->ray.dy;
			env->cam.mapy += (int)env->ray.stepy;
			env->ray.side = 1;
		}
		if (env->map[env->cam.mapx][env->cam.mapy] != '0')
			env->ray.hit = 1;
	}
}

void	init_raycasting(int x, t_env *env)
{
	env->ray.walldist = 0;
	env->ray.hit = 0;
	env->ray.camx = (2 * x / (float)WIN_X) - 1;
	env->ray.posx = env->cam.posx;
	env->ray.posy = env->cam.posy;
	env->cam.mapx = (int)env->cam.posx;
	env->cam.mapy = (int)env->cam.posy;
	env->ray.dirx = env->cam.dirx + env->cam.px * env->ray.camx;
	env->ray.diry = env->cam.diry + env->cam.py * env->ray.camx;
	env->ray.dx = sqrt(1 + ((env->ray.diry * env->ray.diry)
				/ (env->ray.dirx * env->ray.dirx)));
	env->ray.dy = sqrt(1 + ((env->ray.dirx * env->ray.dirx)
				/ (env->ray.diry * env->ray.diry)));
}

void	raycasting(int x, t_env *env)
{
	int		height;

	init_raycasting(x, env);
	pre_calc_wall_dist(env);
	calc_wall_dist(env);
	adjust_view(env);
	height = calc_column_height(env);
	get_texture(env, x, height);
}
