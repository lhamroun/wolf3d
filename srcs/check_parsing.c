/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_parsing.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/21 05:18:23 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:06:38 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		check_if_map_is_surrounded(t_env *env)
{
	int		i;
	int		j;

	i = -1;
	j = 0;
	while (env->map[0][++i])
	{
		if (env->map[0][i] != '1' && env->map[0][i] != ',')
			return (-1);
	}
	i = 1;
	while (env->map[i + 1])
	{
		if (env->map[i][0] != '1'
			&& env->map[i][ft_strlen(env->map[i]) - 1] != ',')
			return (-1);
		++i;
	}
	while (env->map[i][j])
	{
		if (env->map[i][j] != '1' && env->map[i][j] != ',')
			return (-1);
		++j;
	}
	return (1);
}
