/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/21 03:25:17 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:09:07 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		count_nb(char *buf)
{
	int		i;
	int		end;
	int		n;

	i = 0;
	n = 0;
	end = ft_strchr(buf, '\n') - buf;
	if (end == 0 || end >= ft_strlen(buf))
		return (-1);
	while (i < end)
	{
		if (buf[i] == '0' || buf[i] == '1')
			++n;
		++i;
	}
	return (n < 3 ? -1 : n);
}

int		wrong_char(char c)
{
	if (c != '\n' && c != ',' && c != '0' && c != '1')
		return (-1);
	return (1);
}

int		check_map(char *buf, t_env *env, int i)
{
	int		tmp;
	int		nb;

	nb = 0;
	env->n = count_nb(buf);
	while (buf[i])
	{
		tmp = 0;
		if (buf[i] != '1')
			return (-1);
		while (buf[i] && buf[i] != '\n')
		{
			if (wrong_char(buf[i]) == -1)
				return (-1);
			if (buf[i] == '0' || buf[i] == '1')
				++tmp;
			++i;
		}
		if (tmp != env->n || buf[i - 1] != '1')
			return (-1);
		++nb;
		++i;
	}
	return (nb > 2 ? nb : -1);
}

int		store_map(int *j, int i, char *buf, t_env *env)
{
	int		k;

	k = 0;
	if (!(env->map[i] = (char *)ft_memalloc(sizeof(char) * env->n + 1)))
		return (-1);
	while (buf[*j] && buf[*j] != '\n')
	{
		if (ft_isdigit(buf[*j]) == 1)
			env->map[i][k++] = buf[*j];
		*j += 1;
	}
	env->map[i][k] = '\0';
	*j += 1;
	return (1);
}

int		parsing(int fd, t_env *env)
{
	int		i;
	int		j;
	int		nb_line;
	char	*buf;

	i = 0;
	j = 0;
	buf = read_fd(fd);
	if ((nb_line = check_map(buf, env, 0)) == -1)
		return (-1);
	if (!(env->map = (char **)ft_memalloc(sizeof(char *) * nb_line + 1)))
		return (-1);
	while (i < nb_line)
	{
		if (store_map(&j, i, buf, env) == -1)
			return (-1);
		++i;
	}
	if (check_if_map_is_surrounded(env) == -1)
		return (-1);
	env->map[i] = NULL;
	return (1);
}
