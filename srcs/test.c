/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/21 04:28:18 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/08 08:22:39 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	print_camera(t_env *env)
{
	t_camera	cam;

	cam = env->cam;
	printf("--------------------------- CAMERA ---------------------------\n");
	printf("mapx  %d\n", cam.mapx);
	printf("mapy  %d\n", cam.mapy);
	printf("posx  %f\n", cam.posx);
	printf("posy  %f\n", cam.posy);
	printf("dirx  %f\n", cam.dirx);
	printf("diry  %f\n", cam.diry);
	printf("px    %f\n", cam.px);
	printf("py    %f\n", cam.py);
	printf("--------------------------------------------------------------\n");
}

void	print_texture(t_env *env)
{
	(void)env;
	printf("--------------------------- TEXTURE --------------------------\n");
	printf("--------------------------------------------------------------\n");
}

void	print_ray(t_env *env)
{
	t_ray	ray;

	ray = env->ray;
	printf("----------------------------- RAY ----------------------------\n");
	printf("start    %d\n", ray.start);
	printf("end      %d\n", ray.end);
	printf("stepx    %d\n", ray.stepx);
	printf("stepy    %d\n", ray.stepy);
	printf("hit      %d\n", ray.hit);
	printf("side     %d\n", ray.side);
	printf("camx     %f\n", ray.camx);
	printf("walldist %f\n", ray.walldist);
	printf("posx     %f\n", ray.posx);
	printf("posy     %f\n", ray.posy);
	printf("dirx     %f\n", ray.dirx);
	printf("diry     %f\n", ray.diry);
	printf("distx    %f\n", ray.distx);
	printf("disty    %f\n", ray.disty);
	printf("dx       %f\n", ray.dx);
	printf("dy       %f\n", ray.dy);
	printf("--------------------------------------------------------------\n");
}

void	print_env(t_env *env)
{
	int		i;
	int		j;

	i = 0;
	printf("------------------------ PRINT_ENV ---------------------------\n");
	while (env->map[i])
	{
		j = 0;
		while (env->map[i][j])
		{
			printf("%c ", env->map[i][j]);
			++j;
		}
		printf("\n");
		++i;
	}
	print_camera(env);
	print_ray(env);
	printf("--------------------------------------------------------------\n");
}
